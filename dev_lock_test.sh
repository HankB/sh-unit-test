#!/bin/sh
# Bourne shell unit tests using shunit2
# installed on Debian Stretch using apt (2.1.6-1.1)
#
# These  tests generally take a while to run so this is a scaffold
# used to develop new tests. They will then be moved to a different 
# test script once working.

# load lock procedures (if needed)
. ./lock.sh

# test that script acquires resource, holds for $HOLD seconds and
# releases it. HOLD needs to be <= 2 since there is 1 s slop in the
# comparison
hold=2
LOCK_NAME="dead_lock"

# Load shUnit2.
. shunit2
