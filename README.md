# Provide locking service Bourne shell scripts

## Purpose

Several, more or less in order

1) Explore Gitlab
1) Get some work done on scripts used in ZFSsupport project ([https://github.com/HankB/ZFSsupport])
1) Explore unit testing for shell scripts using shUnit2.

This project began originally as `sh-unit-test` and has now been renamed more appropriately.

## TODO

* Provide more testing of lock functionality.
* Rework detection of dead locks (check for active task with matching PID.)

## Overview

`lock.sh` provides interlocks between scripts that may access the same
resource or otherwise need to block until another script completes some task.
It is implicit that the lock is relative to the process. If the process exits
w/out releasing the lock, the next process that tries to acquire it will
succeed (as the PID recorded in the lock file will no longer be executing.)

## Status

Generally complete.

* `slow_test_lock_shunit2.sh` not executing correctly, 2 tests OK ... Hmmm... working now.
* `test_lock_shunit2.sh` 3 tests OK

## Pieces

* `lock.sh` Shell script meant to be sourced by a script that requires locking service.
* `slow_test_lock_shunit2.sh` Perform tests that take some time.
* `test_lock_shunit2.sh` Perform tests that do not require a delay.
* `test_lock.sh` helper used in `test_lock_shunit2.sh`
* `grab_lock.sh` helper used in `slow_test_lock_shunit2.sh`
* `dev_lock_test.sh` scaffold to use for developing new tests before moving to one of `test_lock*.sh`

## Requirements

`shunit2` package (`apt install shunit2` on Debian Stretch.)

## Usage

TBD

## Testing

``` shell
./test_lock_shunit2.sh
./slow_test_lock_shunit2.sh   # takes a while to run
```