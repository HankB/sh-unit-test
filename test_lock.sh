#!/bin/sh
#
# Helper to test the scripts in lock.sh
#
# required arguments lock_name time_to_wait_min time_to_hold_sec

# suggeted test command
# during the test watch contants of  /tmp/lock_test/pid. It should reflect the 
# PID of the first process, go  empty and then reflect the second PID.
# to test cleanup, interrupt the second  process or kill the first while it holds
# the lock
# ./test_lock.sh lock_test 1 10 &  ./test_lock.sh lock_test 1 10

# This second test holds the lock longer than the  second process will wait 
# so only one PID should appear and then clear after the script exits
# ./test_lock.sh lock_test 1 70 &  ./test_lock.sh lock_test 1 70

if [ $# -ne 3 ]
then
    echo  "Usage $0 lock_name time_to_wait_min time_to_hold_sec"
    exit 1
fi

DIR=$1
WAIT=$2
HOLD=$3

#echo DIR $DIR WAIT $WAIT HOLD $HOLD

. `dirname $0`/lock.sh || exit 1


cleanup()
{
  rm  -rf /tmp/$DIR
  exit 1
}

trap cleanup 1 2 3 6

if ! acquireLock $DIR $WAIT
then
    exit 1
fi

sleep $HOLD

releaseLock $DIR

exit 0


