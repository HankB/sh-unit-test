#!/bin/sh
#
# Helper to test the scripts in lock.sh
#
# required arguments lock_name time_to_wait_min 

# Grab a lock and exit w/out releasing so it becomes a dead lock

if [ $# -ne 2 ]
then
    echo  >&2 "Usage $0 lock_name time_to_wait_min"
    exit 1
fi

DIR=$1
WAIT=$2


. `dirname $0`/lock.sh || exit 1


if acquireLock $DIR $WAIT
then
    exit 0
else
    exit 1
fi



