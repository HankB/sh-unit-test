#!/bin/sh
# Bourne shell unit tests using shunit2
# installed on Debian Stretch using apt (2.1.6-1.1)
#
# These are slow running tests and segreated into a different file
# to avoid annoyiong delays during test script development

# load lock procedures (if needed)
. ./lock.sh

hold=2
LOCK_NAME="slow_lock_test"
POLL_INTERVAL=15

# Test normal processing by
# - Acquire a lock
# - Start a subprocess to acquire/relerase the same lock
# - verify that the subprocess is still running (presumably waiting)
# - release the lock
# - verify that the subprocess exited (after acquiring and releasing the lock)
testSubprocessInterlock() {
    acquireLock $LOCK_NAME 1 || fail "Cannot acquire /tmp/$LOCK_NAME/pid"
    ./test_lock.sh $LOCK_NAME 1 $hold &
    subprocess=$!
    sleep `expr $hold \* 2`     # give subprocess time to try
    if ! ps -q  $subprocess >/dev/null    # process not still running?
    then
        fail "subprocess not waiting"
    fi
    releaseLock $LOCK_NAME
    sleep `expr $POLL_INTERVAL + 2 \* $hold`
    if ps -q  $subprocess >/dev/null    # process not still running?
    then
        fail "subprocess not exiting"
    fi
}


# fabricate lock and verify it is not acquired
# there is a minimum 1 minute wait for the lock
testNoAcquire() {
    mkdir /tmp/$LOCK_NAME/
    echo $$ > /tmp/$LOCK_NAME/pid   # create fake lock with our PID
    start=`date +%s`
    shouldfin=`expr $start + 60`
    if ./test_lock.sh $LOCK_NAME 1 $hold
    then
        fail "acquired /tmp/lock_test/pid "
    fi
    fin=`date +%s`

    lower_limit=`expr $shouldfin - 1`
    upper_limit=`expr $shouldfin + 1`
    assertTrue "hold time not in limit of $lower_limit to $upper_limit"\
        "[ $fin -ge $lower_limit -a $fin -le $upper_limit ]"
    
    rm -rf /tmp/$LOCK_NAME/      # remove fake lock
}


# Load shUnit2.
. shunit2
